grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

prog
    : (stat 
    | endState)+ EOF!;

stat
    : expression NL -> expression
    | VAR ID PODST expression NL -> ^(VAR ID) ^(PODST ID expression)
    | VAR ID NL -> ^(VAR ID)
    | ID PODST expression NL -> ^(PODST ID expression)
    | PRINT expression NL -> ^(PRINT expression)
    | ifElse NL -> ifElse
    | NL ->
    ;
    
block
    :
    BLOCKSTART^ (block | stat)* BLOCKEND!
    ;
        
endState
    : LB
    ( stat
    | endState)+
    RB
    NL!
    ;
    


expression
    : multExpr
    ( PLUS^ multExpr
    | MINUS^ multExpr
    )*
    ;

multExpr
    : atom
    ( MUL^ atom
    | DIV^ atom
    | MOD^ atom
    | POW^ atom
    )*
    ;
    

atom
    : INT
    | ID
    | LP! expression RP!
    ;

ifElse
    : IF^ ifExpr THEN! (block | stat) (ELSE! (block | stat))?
    ;

ifExpr
    : expression
      ( EQ^ expression 
      | NEQ^ expression
      )?
    ;      

VAR : 'var';

IF :'if';

ELSE:'else';

THEN:'then';

PRINT : 'print';

ID : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;

INT : '0'..'9'+;

NL : '\r'? '\n' ;

WS : (' ' | '\t')+ {$channel = HIDDEN;} ;

LP
  : '('
  ;

RP
  : ')'
  ;

BLOCKSTART
  : '{'
  ;

BLOCKEND
  : '}'
  ;
  
EQUAL
  : '=='
  ;
  
NEQUAL
  : '!='
  ;  

PODST
  : '='
  ;

PLUS
  : '+'
  ;

MINUS
  : '-'
  ;

MUL
  : '*'
  ;

DIV
  : '/'
  ;
  
MOD
  : '%'
  ;

POW
  : '^'
  ;
  

