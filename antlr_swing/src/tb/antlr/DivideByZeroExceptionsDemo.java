package tb.antlr;


import java.lang.ArithmeticException;
import java.util.InputMismatchException;
import java.util.Scanner;



public class DivideByZeroExceptionsDemo
{
	
	public class DivideByZeroException
    	extends ArithmeticException 
    {

		private static final long serialVersionUID = 1L;
		String message;

		public DivideByZeroException( String message )
		{
			this.message = message;
		}

		public String toString()
		{
			return( message );
		}
	}
	
	public static void main(String[] args)
	{
		DivideByZeroExceptionsDemo demo = new DivideByZeroExceptionsDemo();
		demo.go();
	}


	public void go()
	{
		Scanner s = new Scanner(System.in);

		int numerator, denomenator, result;

		try
			{
				System.out.println("Input the numerator:");
				numerator = s.nextInt();


				System.out.println("Input the denomenator:");
				denomenator = s.nextInt();
				
				result = quotient( numerator, denomenator );
				System.out.println( "The results is " + result );
			}
			// this block is entered if an InputMismatchException is thrown
			catch ( InputMismatchException ime )
			{
				System.out.println("You must enter an integer");
			}
			// this block is entered if an DivideByZeroException is thrown
			catch ( DivideByZeroException dbze )
			{
				System.out.println( dbze.toString() );
			}

	}

   public int quotient( int numerator, int denominator )
      throws DivideByZeroException
   {
	   if ( denominator == 0 )
         throw new DivideByZeroException("You can't divide by zero.");

      return ( numerator / denominator );
   }


}
