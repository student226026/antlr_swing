package tb.antlr.interpreter;
import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;
import tb.antlr.symbolTable.LocalSymbols;
import tb.antlr.DivideByZeroExceptionsDemo.DivideByZeroException;
import tb.antlr.symbolTable.GlobalSymbols;

public class MyTreeParser extends TreeParser {
	
	private LocalSymbols mem = new LocalSymbols();
	private GlobalSymbols globMem = new GlobalSymbols();

    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
	protected void declVar(String id) {
		if(mem.hasSymbol(id)) {
			throw new RuntimeException("variable is declared: "+id);
		}
		mem.newSymbol(id);
	}
	
	protected Integer setVar(String id, Integer value) {
		if(mem.hasSymbol(id)) {
			return mem.setSymbol(id, value);			
		}
		globMem.setSymbol(id, value);
		return value;
	}
	
	protected Integer getVar(String id) {
		if(!mem.hasSymbol(id) && !globMem.hasSymbol(id)) {
			throw new RuntimeException("variable is not declared: "+id);
		} else if(mem.hasSymbol(id)) {
			return mem.getSymbol(id);
		}
		return globMem.getSymbol(id);
	}
	
	protected Integer mod(Integer e1, Integer e2) {
		if (e2 == 0) {
			throw new ArithmeticException("mod == 0");
		}
		return e1%e2;
	}
	
	protected Integer div(Integer e1, Integer e2) throws DivideByZeroException{
		if (e2 == 0) {
			throw new ArithmeticException("denominator == 0");
		}
		return (int) e1/e2;
	}
	
	protected Integer pow(Integer e1, Integer e2) {
		return (int) Math.pow(e1, e2);
	}
	
	protected Integer enterScope() {
		return mem.enterScope();
	}
	
	protected Integer leaveScope() {
		return mem.leaveScope();
	}
	
	protected void declGlobVar(String id) {
		if(globMem.hasSymbol(id)) {
			throw new RuntimeException("declared variable: "+id);
		}
		
		globMem.newSymbol(id);
	}


	protected Integer add(Integer e1, Integer e2) {
		return e1 + e2;
	}


	protected Integer subtract(Integer e1, Integer e2) {
		return e1 - e2;
	}	

	protected Integer multiply(Integer e1, Integer e2) {
		return e1 * e2;
	}

	protected Integer divide(Integer e1, Integer e2) throws Exception {
		if(e2 == 0)
			throw new Exception("Nie mozna dzielic przez 0!");
		return e1 / e2;
	}

	protected Integer magnify(Integer e1, Integer e2) {
		return (int) Math.pow(e1, e2);
	}

	protected void createVar (String nazwa) {
		globMem.newSymbol(nazwa);
	}

}
