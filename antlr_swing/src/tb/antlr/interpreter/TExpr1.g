tree grammar TExpr1;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
}

prog    : (expression
        | print
        | globVar
        | endState)*
        ;

expression returns [Integer out]
        : ^(PLUS  e1=expression e2=expression) {$out = $e1.out + $e2.out;}
        | ^(MINUS e1=expression e2=expression) {$out = $e1.out - $e2.out;}
        | ^(MUL   e1=expression e2=expression) {$out = $e1.out * $e2.out;}
        | ^(DIV   e1=expression e2=expression) {$out = div($e1.out, $e2.out);}
        | ^(MOD   e1=expression e2=expression) {$out = mod($e1.out, $e2.out);}
        | ^(POW   e1=expression e2=expression) {$out = pow($e1.out, $e2.out);}
        | ^(PODST i1=ID   e2=expression) {$out = setVar($i1.text, $e2.out);}
        | ^(AND e1=expr e2=expr){$out = bitAND($e1.out, $e2.out);}
        | ^(OR e1=expr e2=expr){$out = bitOR($e1.out, $e2.out);}        
        | ID                       {$out = getVar($ID.text);}
        | INT                      {$out = getInt($INT.text);}
        ;
        
declaration:      ^(VAR i1=ID) {createVar($i1.text);};
          
print   : ^(PRINT e=expression) {drukuj ($e.text + " = " + $e.out.toString());}
        ;

var     : ^(VAR id=ID) {declVar($id.text);}
        ;

globVar : ^(VAR id=ID) {declGlobVar($id.text);}
        ;

endState
        : (LB {enterScope();}
        ( expression
        | print
        | var
        | endState)*
        RB {leaveScope();})
        ;